<?php

namespace Drupal\openedu_subtheme\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subtheme Entity entities.
 *
 * @ingroup openedu_subtheme
 */
class SubthemeEntityDeleteForm extends ContentEntityDeleteForm {


}
